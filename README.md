Chained-CI
==========

Role
----
Chained-CI is a way to run a set of projects, each one as a job in a top level
pipeline.

This project, running on gitlab CE, is triggering configured projects one
after the other, or in parallele, sharing configuration through artifacts. This
allow to integrate projects managed by third parties, or running together
independant projects.

___This project is hosting the pipelines and the pipeline configuration.
See https://gitlab.forge.orange-labs.fr/osons/chained-ci-roles to see the
roles needed to run thoses pipelines___

Input
-----
  - Environment variables:
    - __Required__:
      - POD:
          - role: pod name as defined in pod_inventory/inventory
          - example: pod1
          - default: none
    - Optional:
      - AREYOUSURE:
        - role: disable the deployment protection on some pods
        - default: ''
        - values: '' or 'MAIS OUI !!!'
      - ansible_verbose:
          - role: verbose option for ansible
          - values: "", "-vvv"
          - default: ""
      - infra_branch:
          - role: the branch for project infra_manager
          - default: master
      - os_infra_branch:
          - role: the branch for project os_infra_manager
          - default: master
      - vim_branch:
          - role: the branch for the VIMs projects (kolla, k8s)
          - default: master
      - kolla_branch:
          - role: a value to pass to project kolla
          - default: 'stable/queens'
      - kolla_ansible_branch:
          - role: a value to pass to project kolla
          - default: 'stable/queens'
      - functest_branch:
          - role: the branch for project functest
          - default: master
      - acumos_branch:
          - role: the branch for project acumos-installer
          - default: master

Output
------
  - artifacts: each step can fetch the artifact generate by the sub-project as
      defined in the git_projects variables pull_artifacts

Vue
------
A web interface is available in the pages of this project.
